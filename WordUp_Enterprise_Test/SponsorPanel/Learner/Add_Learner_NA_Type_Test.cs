﻿namespace WordUp_Enterprise_Test
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Pangolin;
    using System;


    [TestClass]
    public class Add_Learner_NA_Type_Test : UITest
    {
        [TestMethod]
        public override void RunTest()
        {

            LoginAs<SponsorLoginTest>();
            ClickLink("Learners");

            WaitForNewPage();

            ClickButton("New Learner");


            Set("First name").To("Firstname1");

            Set("Last name").To("Lastname1");

            Set("Email").To("firstname1.lastname1@test.com");

            ClickLabel("N/A");


            ClickButton("Save");
            WaitForNewPage();
            WaitToSeeRow(That.Contains, "Firstname1 Lastname1");
        }
    }
}
