﻿namespace WordUp_Enterprise_Test
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Pangolin;
    using System;


    [TestClass]
    public class Add_Learner_ProLifetime_Type_Test : UITest
    {
        [TestMethod]
        public override void RunTest()
        {
            LoginAs<SponsorLoginTest>();
            ClickLink("Learners");

            WaitForNewPage();

            ClickButton("New Learner");


            Set("First name").To("Firstname5");

            Set("Last name").To("Lastname5");

            Set("Email").To("firstname5.lastname5@test.com");

            ClickLabel("Pro Lifetime");

            ClickButton("Save");
            WaitForNewPage();
            WaitToSeeRow(That.Contains, "Firstname5 Lastname5");
        }
    }
}
