﻿namespace WordUp_Enterprise_Test
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Pangolin;
    using System;


    [TestClass]
    public class Add_Learner_ProAnnual_Type_Test : UITest
    {
        [TestMethod]
        public override void RunTest()
        {

            LoginAs<SponsorLoginTest>();
            ClickLink("Learners");

            WaitForNewPage();

            ClickButton("New Learner");


            Set("First name").To("Firstname4");

            Set("Last name").To("Lastname4");

            Set("Email").To("firstname4.lastname4@test.com");

            ClickLabel("Pro Annual");

            ClickButton("Save");
            WaitForNewPage();
            WaitToSeeRow(That.Contains, "Firstname4 Lastname4");
        }
    }
}
