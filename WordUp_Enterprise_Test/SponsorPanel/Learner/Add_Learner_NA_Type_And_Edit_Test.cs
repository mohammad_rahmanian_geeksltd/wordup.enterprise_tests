﻿namespace WordUp_Enterprise_Test
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Pangolin;
    using System;


    [TestClass]
    public class Add_Learner_NA_Type_And_Edit_Test : UITest
    {
        [TestMethod]
        public override void RunTest()
        {

            Run<Add_Learner_NA_Type_Test>();

            AtRow(That.Contains, "Firstname1 Lastname1").ClickLink("Edit");



            WaitForNewPage();



            Set("First name").To("Firstname2");

            Set("Last name").To("Lastname2 Edited");

            Set("Email").To("firstname3.lastname3@test.com");

            ClickLabel("N/A");


            ClickButton("Save");

            WaitForNewPage();

            WaitToSee(What.Contains, "Firstname2 Lastname2 Edited");
        }
    }
}
