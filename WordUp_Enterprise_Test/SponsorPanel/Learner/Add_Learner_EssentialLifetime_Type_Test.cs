﻿namespace WordUp_Enterprise_Test
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Pangolin;
    using System;


    [TestClass]
    public class Add_Learner_EssentialLifetime_Type_Test : UITest
    {
        [TestMethod]
        public override void RunTest()
        {

            LoginAs<SponsorLoginTest>();
            ClickLink("Learners");

            WaitForNewPage();

            ClickButton("New Learner");


            Set("First name").To("Firstname3");

            Set("Last name").To("Lastname3");

            Set("Email").To("firstname3.lastname3@test.com");

            ClickLabel("Essential Lifetime");

            ClickButton("Save");
            WaitForNewPage();
            WaitToSeeRow(That.Contains, "Firstname3 Lastname3");
        }
    }
}
