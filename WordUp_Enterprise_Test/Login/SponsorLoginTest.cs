﻿namespace WordUp_Enterprise_Test
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Pangolin;
    using System;


    [TestClass]
    public class SponsorLoginTest : UITest
    {
        [TestMethod]
        public override void RunTest()
        {
            Logout();

            Goto("/login");
            Set("Email").To("sponsor@uat.co");
            Set("Password").To("test");
            Click("Login");
            WaitForNewPage();
            // Expect("Logout");

        }
    }
}
