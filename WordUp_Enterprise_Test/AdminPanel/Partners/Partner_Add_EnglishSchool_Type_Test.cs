﻿namespace WordUp_Enterprise_Test
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Pangolin;
    using System;


    [TestClass]
    public class Partner_Add_EnglishSchool_Type_Test : UITest
    {
        [TestMethod]
        public override void RunTest()
        {
            LoginAs<AdminLoginTest>();

            ClickLink("Partners");
            ClickLink(That.Equals, "New Partner");

            ClickLabel("EnglishSchool");

            Set("Name").To("English School 1");

            Set("Primary contact").To("Contact No1");

            Set("Email").To("test1@test.com");

            Set("Notes").To("Lorem Ipsum is simply dummy text of the printing and typesetting industry.");


            Set("Sub-domain").To("sub1");

          
            Set(The.Right, "Choose file").To("logo.jpg");

            Set(The.Bottom, "Choose file").To("sample.jpg");

            Set("Title").To("Title1");

            Set("About us").To("Lorem Ipsum is simply dummy text of the printing and typesetting industry.");

            Set("Price discount").To("10");

            Set("Commission").To("20");

            Set("Commission payment details").To("Lorem Ipsum is simply dummy text of the printing and typesetting industry.");


            ClickButton("Save");
            WaitForNewPage();
            WaitToSeeRow(That.Contains, "English School 1");
        }
    }
}
