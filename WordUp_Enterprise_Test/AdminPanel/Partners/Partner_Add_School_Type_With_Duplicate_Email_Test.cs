﻿namespace WordUp_Enterprise_Test
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Pangolin;
    using System;


    [TestClass]
    public class Partner_Add_School_Type_With_Duplicate_Email_Test : UITest
    {
        [TestMethod]
        public override void RunTest()
        {
            LoginAs<AdminLoginTest>();

            Run<Partner_Add_School_Type_Test>();

            ClickLink("Partners");
            ClickLink(That.Equals, "New Partner");

            ClickLabel("Other");

            Set("Name").To("Other 1");

            Set("Primary contact").To("Contact No1");

            Set("Email").To("test5@test.com");

            Set("Notes").To("Lorem Ipsum is simply dummy text of the printing and typesetting industry.");

            Set("Sub-domain").To("sub7");

            Set("Title").To("Title1");

            Set("About us").To("Lorem Ipsum is simply dummy text of the printing and typesetting industry.");

            Set("Price discount").To("10");

            Set("Commission").To("20");

            Set("Commission payment details").To("Lorem Ipsum is simply dummy text of the printing and typesetting industry.");


            ClickButton("Save");

            WaitToSee(What.Contains, "Email must be unique");
        }
    }
}
