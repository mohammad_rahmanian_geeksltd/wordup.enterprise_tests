﻿namespace WordUp_Enterprise_Test
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Pangolin;
    using System;


    [TestClass]
    public class Partner_Add_Influencer_Type_Test : UITest
    {
        [TestMethod]
        public override void RunTest()
        {
            LoginAs<AdminLoginTest>();

            ClickLink("Partners");
            ClickLink(That.Equals, "New Partner");

            ClickLabel("Influencer");

            Set("Name").To("Influencer 1");

            Set("Primary contact").To("Contact No1");

            Set("Email").To("test3@test.com");

            Set("Notes").To("Lorem Ipsum is simply dummy text of the printing and typesetting industry.");

            Set("Sub-domain").To("sub2");

            Set(The.Right, "Choose file").To("logo.jpg");

            Set(The.Bottom, "Choose file").To("sample.jpg");

            Set("Title").To("Title1");

            Set("About us").To("Lorem Ipsum is simply dummy text of the printing and typesetting industry.");

            Set("Price discount").To("10");

            Set("Commission").To("20");

            Set("Commission payment details").To("Lorem Ipsum is simply dummy text of the printing and typesetting industry.");


            ClickButton("Save");
            WaitForNewPage();
            WaitToSeeRow(That.Contains, "Influencer 1");
        }
    }
}
