﻿namespace WordUp_Enterprise_Test
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Pangolin;
    using System;


    [TestClass]
    public class Sponsor_Add_Employer_Type_And_Edit_Test : UITest
    {
        [TestMethod]
        public override void RunTest()
        {
            LoginAs<AdminLoginTest>();
            Run<Sponsor_Add_Univerisity_Type_Test>();

            WaitForNewPage();
            ClickLink(That.Contains, "University");
            WaitForNewPage();
            ClickLink(That.Contains, "Edit");

            ClearField("Date registered");
            Set("Date registered").To("20/07/2021");

            ClickLabel("Employer");

            Set("Name").To("University Test1 Edited");

            Set("Domains").To("Domain1,Domain2,Domain3");

            Set("Total licenses").To("10");

            Set("Primary contact").To("Contact No1");

            Set("Email").To("University2@test.com");

            Set("Renewal date").To("250/08/2021");

            Set("Notes").To("Lorem Ipsum is simply dummy text of the printing and typesetting industry.");

            ClickButton("Save");
            WaitToSee("University Test1 Edited");

        }
    }
}
