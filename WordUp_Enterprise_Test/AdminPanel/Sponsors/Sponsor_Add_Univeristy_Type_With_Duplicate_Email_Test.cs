﻿namespace WordUp_Enterprise_Test
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Pangolin;
    using System;


    [TestClass]
    public class Sponsor_Add_Univeristy_Type_With_Duplicate_Email_Test : UITest
    {
        [TestMethod]
        public override void RunTest()
        {
            LoginAs<AdminLoginTest>();

            Run<Sponsor_Add_Univerisity_Type_Test>();

            ClickLink("Sponsors");
            ClickLink(That.Equals, "New Sponsor");

            Set("Date registered").To("20/08/2021");

            ClickLabel("University");

            Set("Name").To("University Test1");

            Set("Domains").To("Domain1,Domain2,Domain3");

            Set("Total licenses").To("1");

            Set("Primary contact").To("Contact No1");

            Set("Email").To("University1@test.com");

            Set("Renewal date").To("30/08/2021");

            Set("Notes").To("Lorem Ipsum is simply dummy text of the printing and typesetting industry.");


            ClickButton("Save");

            WaitToSee(What.Contains, "Email must be unique");
        }
    }
}
