﻿namespace WordUp_Enterprise_Test
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Pangolin;
    using System;


    [TestClass]
    public class Learner_Add_Sponsor_Type_Other_And_Add_Learner_Type_ProLifetime_Test : UITest
    {
        [TestMethod]
        public override void RunTest()
        {
            LoginAs<AdminLoginTest>();
            Run<Sponsor_Add_Other_Type_Test>();

            ClickLink(That.Contains, "Other");

            Run<Learner_Add_ProLifetime_Type>();
        }
    }
}
