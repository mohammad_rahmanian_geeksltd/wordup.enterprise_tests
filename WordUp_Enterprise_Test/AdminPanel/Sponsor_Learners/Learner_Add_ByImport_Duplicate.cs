﻿namespace WordUp_Enterprise_Test
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Pangolin;
    using System;


    [TestClass]
    public class Learner_Add_ByImport_Duplicate : UITest
    {
        [TestMethod]
        public override void RunTest()
        {

            ClickLink("Users");

            ClickButton("Import");



            Set("Choose file").To("users_duplicated.CSV");


            ClickButton("Import");
            // WaitForNewPage();
            // WaitToSeeRow(That.Contains, "Firstname5 Lastname5");
        }
    }
}
