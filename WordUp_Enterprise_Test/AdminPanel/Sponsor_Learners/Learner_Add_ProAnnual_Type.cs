﻿namespace WordUp_Enterprise_Test
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Pangolin;
    using System;


    [TestClass]
    public class Learner_Add_ProAnnual_Type : UITest
    {
        [TestMethod]
        public override void RunTest()
        {

            ClickLink("Users");

            ClickButton("New Learner");


            Set("First name").To("Firstname4");

            Set("Last name").To("Lastname4");

            Set("Email").To("firstname4.lastname4@test.com");

            ClickLabel("Pro Annual");

            ClickButton("Save");
            WaitForNewPage();
            WaitToSeeRow(That.Contains, "Firstname4 Lastname4");
        }
    }
}
