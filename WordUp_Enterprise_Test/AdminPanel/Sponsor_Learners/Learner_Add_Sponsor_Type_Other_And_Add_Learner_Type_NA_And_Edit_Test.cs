﻿namespace WordUp_Enterprise_Test
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Pangolin;
    using System;


    [TestClass]
    public class Learner_Add_Sponsor_Type_Other_And_Add_Learner_Type_NA_And_Edit_Test : UITest
    {
        [TestMethod]
        public override void RunTest()
        {
            LoginAs<AdminLoginTest>();
            Run<Sponsor_Add_Other_Type_Test>();

            ClickLink(That.Contains, "Other");


            Run<Learner_Add_NA_Type>();

            ClickLink(That.Contains, "Firstname1 Lastname1");

            WaitForNewPage();

            ClickLink(That.Contains, "Edit");

            Set("First name").To("Firstname2");

            Set("Last name").To("Lastname2");

            Set("Email").To("firstname3.lastname3@test.com");

            ClickLabel("N/A");


            ClickButton("Save");

            WaitForNewPage();

            WaitToSee(What.Contains, "Firstname2 Lastname2");
        }
    }
}
