﻿namespace WordUp_Enterprise_Test
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Pangolin;
    using System;


    [TestClass]
    public class Learner_Add_Sponsor_Type_University_And_Add_Learner_Licence_Limit_Test : UITest
    {
        [TestMethod]
        public override void RunTest()
        {
            LoginAs<AdminLoginTest>();
            Run<Sponsor_Add_Univerisity_Type_Test>();

            ClickLink(That.Contains, "University");

            Run<Learner_Add_NA_Type>();


            ClickButton("New Learner");

            WaitToSee(What.Contains, "License limit is reached");
        }
    }
}
