﻿namespace WordUp_Enterprise_Test
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Pangolin;
    using System;


    [TestClass]
    public class Learner_Add_ByImport : UITest
    {
        [TestMethod]
        public override void RunTest()
        {

            ClickLink("Users");

            ClickButton("Import");



            Set("Choose file").To("users.CSV");


            ClickButton("Import");
            WaitForNewPage();
            // WaitToSeeRow(That.Contains, "Firstname5 Lastname5");
        }
    }
}
