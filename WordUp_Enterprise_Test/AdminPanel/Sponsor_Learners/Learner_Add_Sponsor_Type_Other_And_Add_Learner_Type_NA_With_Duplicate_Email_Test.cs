﻿namespace WordUp_Enterprise_Test
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Pangolin;
    using System;


    [TestClass]
    public class Learner_Add_Sponsor_Type_Other_And_Add_Learner_Type_NA_With_Duplicate_Email_Test : UITest
    {
        [TestMethod]
        public override void RunTest()
        {
            LoginAs<AdminLoginTest>();
            Run<Sponsor_Add_Other_Type_Test>();

            ClickLink(That.Contains, "Other");


            Run<Learner_Add_NA_Type>();

            ClickLink("Users");

            ClickButton("New Learner");

            Set("First name").To("Firstname1");

            Set("Last name").To("Lastname1");

            Set("Email").To("firstname1.lastname1@test.com");

            ClickLabel("N/A");


            ClickButton("Save");

            WaitToSee(What.Contains, "Email must be unique");
        }
    }
}
