﻿namespace WordUp_Enterprise_Test
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Pangolin;
    using System;


    [TestClass]
    public class Learner_Add_Sponsor_Type_Employer_And_Add_Learner_ByImport_Duplicate_Email_Test : UITest
    {
        [TestMethod]
        public override void RunTest()
        {
            LoginAs<AdminLoginTest>();
            Run<Sponsor_Add_Employer_Type_Test>();

            ClickLink(That.Contains, "Employer");

            Run<Learner_Add_ByImport_Duplicate>();

            WaitToSee(What.Contains, "Email must be unique");
        }
    }
}
