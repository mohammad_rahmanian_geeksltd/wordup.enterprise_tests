﻿namespace WordUp_Enterprise_Test
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Pangolin;
    using System;


    [TestClass]
    public class Learner_Add_Sponsor_Type_Employer_And_Add_Learner_Type_EssentialLifetime_Test : UITest
    {
        [TestMethod]
        public override void RunTest()
        {
            LoginAs<AdminLoginTest>();
            Run<Sponsor_Add_Employer_Type_Test>();

            ClickLink(That.Contains, "Employer");

            Run<Learner_Add_EssentialLifetime_Type>();
        }
    }
}
