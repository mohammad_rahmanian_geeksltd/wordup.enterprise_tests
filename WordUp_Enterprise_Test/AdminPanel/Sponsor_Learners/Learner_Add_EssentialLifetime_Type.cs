﻿namespace WordUp_Enterprise_Test
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Pangolin;
    using System;


    [TestClass]
    public class Learner_Add_EssentialLifetime_Type : UITest
    {
        [TestMethod]
        public override void RunTest()
        {

            ClickLink("Users");

            ClickButton("New Learner");


            Set("First name").To("Firstname3");

            Set("Last name").To("Lastname3");

            Set("Email").To("firstname3.lastname3@test.com");

            ClickLabel("Essential Lifetime");

            ClickButton("Save");
            WaitForNewPage();
            WaitToSeeRow(That.Contains, "Firstname3 Lastname3");
        }
    }
}
