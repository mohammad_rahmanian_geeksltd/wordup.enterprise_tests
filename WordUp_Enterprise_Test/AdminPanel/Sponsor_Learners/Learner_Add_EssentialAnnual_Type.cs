﻿namespace WordUp_Enterprise_Test
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Pangolin;
    using System;


    [TestClass]
    public class Learner_Add_EssentialAnnual_Type : UITest
    {
        [TestMethod]
        public override void RunTest()
        {

            ClickLink("Users");

            ClickButton("New Learner");


            Set("First name").To("Firstname2");

            Set("Last name").To("Lastname2");

            Set("Email").To("firstname2.lastname2@test.com");

            ClickLabel("Essential Annual");

            ClickButton("Save");
            WaitForNewPage();
            WaitToSeeRow(That.Contains, "Firstname2 Lastname2");
        }
    }
}
