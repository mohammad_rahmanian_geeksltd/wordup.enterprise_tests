﻿namespace WordUp_Enterprise_Test
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Pangolin;
    using System;


    [TestClass]
    public class Admin_Add_Test_And_Edit_Test : UITest
    {
        [TestMethod]
        public override void RunTest()
        {
            LoginAs<AdminLoginTest>();

            Run<Admin_Add_Test>();

            AtRow(That.Contains, "Firstname1 Lastname1").ClickLink("Edit");

            WaitForNewPage();

            Set("First name").To("Firstname3");

            Set("Last name").To("Lastname Edited");


            Set("Email").To("admin8@test.com");



            ClickButton("Save");
            WaitForNewPage();
            WaitToSeeRow(That.Contains, "Firstname3 Lastname Edited");
        }
    }
}
