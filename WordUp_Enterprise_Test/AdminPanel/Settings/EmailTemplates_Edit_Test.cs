﻿namespace WordUp_Enterprise_Test
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Pangolin;
    using System;


    [TestClass]
    public class EmailTemplates_Edit_Test : UITest
    {
        [TestMethod]
        public override void RunTest()
        {
            LoginAs<AdminLoginTest>();

            ClickLink("Settings");

            WaitForNewPage();

            ClickLink("Email Templates");

            AtRow(That.Contains, "InvitationEmail").ClickLink("Edit");

            WaitForNewPage();


            Set("Subject").To("Subject Edited");

            Set("Supported placeholders").To("[#PartnerUrl#],[#BrandColor#],[#FontColor#],[#Edied#]");


            Set("Body").To("Test Edit");

            ClickButton("Save");
            WaitForNewPage();
            WaitToSeeRow(That.Contains, "Subject Edited");
        }
    }
}
