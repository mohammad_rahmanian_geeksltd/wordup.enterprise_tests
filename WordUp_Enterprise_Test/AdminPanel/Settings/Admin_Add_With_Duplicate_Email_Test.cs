﻿namespace WordUp_Enterprise_Test
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Pangolin;
    using System;


    [TestClass]
    public class Admin_Add_With_Duplicate_Email_Test : UITest
    {
        [TestMethod]
        public override void RunTest()
        {
            LoginAs<AdminLoginTest>();

            Run<Admin_Add_Test>();


            ClickLink("New User");


            Set("First name").To("Firstname2");

            Set("Last name").To("Lastname2");


            Set("Email").To("admin1@test.com");



            ClickButton("Save");
            WaitToSee(What.Contains, "Email must be unique");
        }
    }
}
