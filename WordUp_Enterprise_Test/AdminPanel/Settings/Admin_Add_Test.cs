﻿namespace WordUp_Enterprise_Test
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Pangolin;
    using System;


    [TestClass]
    public class Admin_Add_Test : UITest
    {
        [TestMethod]
        public override void RunTest()
        {
            LoginAs<AdminLoginTest>();

            ClickLink("Settings");

            WaitForNewPage();

            ClickLink("Admin Users");

            ClickLink("New User");


            Set("First name").To("Firstname1");

            Set("Last name").To("Lastname1");


            Set("Email").To("admin1@test.com");



            ClickButton("Save");
            WaitForNewPage();
            WaitToSeeRow(That.Contains, "Firstname1 Lastname1");
        }
    }
}
